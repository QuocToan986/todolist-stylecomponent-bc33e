import React, { Component } from "react";
import { ThemeProvider } from "styled-components";
import { Container } from "../Component/Container";
import { Dropdown } from "../Component/Dropdown";
import { Heading3 } from "../Component/Heading";
import { TextField } from "../Component/TextField";
import { Button } from "../Component/Button"
import { Table, Th, Thead, Tr } from "../Component/Table";
import { connect } from "react-redux";
import { addTaskAction, changeThemeAction } from "../redux/actions/todolistActions";
import { arrTheme } from "../Themes/ThemeManager";
import { change_theme } from "../redux/types/todolistTypes";

class Todolist extends Component {

  //lưu data của input nhập vào
  state = {
    taskName: "",
  }

  //phần render ra tasklist từ store redux
  renderTaskToDone = () => {
    return this.props.taskList.filter(task => !task.done).map((task) => {
      return <Tr key={task.id}>
        <Th style={{ verticalAlign: "middle" }}>{task.taskName}</Th>
        <Th className="text-right">
          <Button className="mr-1"><i className="fa fa-edit"></i></Button>
          <Button className="mr-1"><i className="fa fa-check"></i></Button>
          <Button className="mr-1"><i className="fa fa-trash"></i></Button>
        </Th>
      </Tr>
    })
  }
  renderTaskComplete = () => {
    return this.props.taskList.filter(task => task.done).map((task) => {
      return <Tr key={task.id}>
        <Th style={{ verticalAlign: "middle" }}>{task.taskName}</Th>
        <Th className="text-right">
          <Button className="mr-1"><i className="fa fa-trash"></i></Button>
        </Th>
      </Tr>
    })
  }

  // phần render ra option theme
  renderTheme = () => {
    return arrTheme.map((theme) => {
      return <>
        <option value={theme.id}>{theme.name}</option>
      </>
    })
  }

  render() {
    return <ThemeProvider theme={this.props.themeToDoList}>
      <Container className="text-left my-5">
        {/*Phần chọn theme*/}
        <Dropdown onChange={(e) => {
          // lấy giá trị của selection
          let { value } = e.target
          // dispatch value lên reducer
          this.props.dispatch(changeThemeAction(value))
        }}>
          {this.renderTheme()}
        </Dropdown>

        {/* chữ todolist */}
        <Heading3>To do list</Heading3>
        {/* Phần task name */}
        <TextField onChange={(e) => {
          // lấy data từ input
          this.setState({
            taskName: e.target.value
          }, () => {
            console.log(this.state)
          })
        }} name="taskName" style={{ verticalAlign: "middle" }} className="w-50" label="Task name" />
        <Button onClick={() => {

          // lấy thông tin người dùng
          let { taskName } = this.state

          // tao ra 1 task object
          let newTask = {
            id: Date.now(),
            taskName: taskName,
            done: false
          }
          console.log(newTask)

          // đưa task object lên redux thông qua phương thức dispatch
          this.props.dispatch(addTaskAction(newTask))


        }} style={{ verticalAlign: "middle" }} className="ml-2"><i className="fa fa-plus"></i> Add task</Button>
        <Button style={{ verticalAlign: "middle" }} className="ml-2"><i className="fa fa-upload"></i> UpLoad task</Button>
        <hr className="text-white border" />

        {/* Phần tasktodo */}
        <Heading3>Task to do</Heading3>
        <Table>
          <Thead>
            {this.renderTaskToDone()}
          </Thead>
        </Table>

        {/* Phần taskcomplete */}
        <Heading3>Task complete</Heading3>
        <Table>
          <Thead>
            {this.renderTaskComplete()}
          </Thead>
        </Table>
      </Container>
    </ThemeProvider>;
  }
}

let mapStateToProp = (state) => {
  return {
    themeToDoList: state.todolistReducer.themeToDoList,
    taskList: state.todolistReducer.taskList
  }

}

export default connect(mapStateToProp)(Todolist)