import { dataTaskList } from "../../Data/taskList";
import { arrTheme } from "../../Themes/ThemeManager";
import { ToDoListDarkTheme } from "../../Themes/ToDoListDarkTheme";
import { add_task, change_theme } from "../types/todolistTypes";

let initialState = {
    themeToDoList: ToDoListDarkTheme,
    taskList: dataTaskList
}

export const todolistReducer = (state = initialState, action) => {
    switch (action.type) {
        case add_task: {
            // kiểm tra rỗng
            if (action.newTask.taskName.trim() === "") {
                alert("Task name is required!")
                return { ...state }
            }

            // kiểm tra tồn tại
            let taskListUpdate = [...state.taskList];
            let index = taskListUpdate.findIndex(task => task.taskName === action.newTask.taskName);
            if (index !== -1) {
                alert("Task name already exits");
                return { ...state }
            }
            taskListUpdate.push(action.newTask)

            // xử lý xong taskListUpdate thì gán vào taskList
            state.taskList = taskListUpdate;
            return { ...state }

        }
        case change_theme: {
            console.log(action)
            // tìm theme dựa vào action.themeId được chọn
            let theme = arrTheme.find(theme => theme.id == action.themeId);
            if (theme) {
                // set lại theme cho state.themeToDoList
                state.themeToDoList = { ...theme.theme }
            }
            return { ...state }
        }
        default:
            return state
    }
}